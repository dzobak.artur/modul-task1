const TriangleService = require('../services/grades.service');


async function calculatePerimeter(req, res) {
    try {
        const { A, B, C } = req.body;

        
        const sideAB = Math.sqrt((B.x - A.x) ** 2 + (B.y - A.y) ** 2);
        const sideBC = Math.sqrt((C.x - B.x) ** 2 + (C.y - B.y) ** 2);
        const sideCA = Math.sqrt((A.x - C.x) ** 2 + (A.y - C.y) ** 2);

       
        const perimeter = sideAB + sideBC + sideCA;

       
        const newTriangle = await TriangleService.create({ A, B, C, perimeter });

        res.status(200).json({
            status: 200,
            data: {
                perimeter,
                triangle: newTriangle,
            },
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: 'Помилка обчислення периметра трикутника або збереження даних у базі',
        });
    }
}

module.exports = {
    calculatePerimeter,
};
