const { Schema, model } = require('mongoose');

const triangleSchema = new Schema({
  A: {
    x: { type: Number, required: true },
    y: { type: Number, required: true }
  },
  B: {
    x: { type: Number, required: true },
    y: { type: Number, required: true }
  },
  C: {
    x: { type: Number, required: true },
    y: { type: Number, required: true }
  },
  perimeter: {
    type: Number,
    required: true,
  },
}, {
  timestamps: true,
});

const Triangle = model('Triangle', triangleSchema);

module.exports = Triangle;

