const Triangle = require('../models/grade.model');



async function create({ A, B, C, perimeter }) {
    return Triangle.create({ A, B, C, perimeter });
}

module.exports = {
    create,
};