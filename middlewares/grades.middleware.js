const createError = require('http-errors');

function validateTriangleInput(req, res, next) {
    const { A, B, C } = req.body;

    
    if (!A || !B || !C) {
        return next(createError.BadRequest('Координати всіх вершин трикутника повинні бути задані'));
    }

    
    if (
        isNaN(parseFloat(A.x)) || isNaN(parseFloat(A.y)) ||
        isNaN(parseFloat(B.x)) || isNaN(parseFloat(B.y)) ||
        isNaN(parseFloat(C.x)) || isNaN(parseFloat(C.y))
    ) {
        return next(createError.BadRequest('Координати вершин трикутника повинні бути числовими значеннями'));
    }

    
    const sideAB = Math.sqrt((B.x - A.x) ** 2 + (B.y - A.y) ** 2);
    const sideBC = Math.sqrt((C.x - B.x) ** 2 + (C.y - B.y) ** 2);
    const sideCA = Math.sqrt((A.x - C.x) ** 2 + (A.y - C.y) ** 2);
    if (sideAB <= 0 || sideBC <= 0 || sideCA <= 0) {
        return next(createError.BadRequest('Довжини сторін трикутника повинні бути додатніми числами'));
    }

    
    if (sideAB + sideBC <= sideCA || sideAB + sideCA <= sideBC || sideBC + sideCA <= sideAB) {
        return next(createError.BadRequest('Сума будь-яких двох сторін трикутника повинна бути більше третьої'));
    }

    next();
}

module.exports = {
    validateTriangleInput,
};
