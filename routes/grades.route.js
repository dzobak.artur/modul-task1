const express = require('express');
const router = express.Router();

const controller = require('../controllers/grades.controller'); 
const middleware = require('../middlewares/grades.middleware');

router.route('/calculate')
    //.get(controller.getAllTriangles)
    .post(middleware.validateTriangleInput, controller.calculatePerimeter);


module.exports = router;
